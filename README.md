# default_ev_density



## Repository structure
```
data                    -- containts the dataset in Geotiff format
readme.md               -- Readme file 
datapackage.json        -- Includes the meta information of the dataset for processing and data integration

```


## Documentation

The dataset is optained by running the calculation module "EV Density" on the Hotmaps plateform. 
It is intended to be used in the calculation module "Commuter driven distance" as a default dataset if "EV Density" has not been run before.


### Limitations of the dataset
The dataset only provides default data with the folowing parameters : 

```
vehicles_per_habitant = 0.56
vehicle_urban_factor = 0.2
year = 2050
fleet_renewal_share = 0.06
yearly_factor = 0.06
Share_electric_cars_new_registrations_2020 = 0.1
```

### References 
 - The vehicles per habitant ratio was given by the European Automobile Manufacturer's Association (ACEA) in 2023 [on this page](https://www.acea.auto/figure/motorisation-rates-in-the-eu-by-country-and-vehicle-type/).
 - The ratio between urban and rural factor was obtaiend from data of the Swiss Federal Office for Statistics in 2015 [on this page](https://www.bfs.admin.ch/bfs/fr/home/statistiques/mobilite-transports/transport-personnes/comportements-transports/possession-vehicules-permis-conduire.assetdetail.2004958.html)
 - The fleet renewal share and the share of elevtric car in the new registrations were calculated based on the data of new immatriculations by year in Switzerland up to 2029 [from this page](https://www.bfs.admin.ch/bfs/en/home/statistics/mobility-transport/transport-infrastructure-vehicles/vehicles/road-new-registrations.assetdetail.20884460.html)
 - The EV share in the actual fleet was also obtained from the ACEA in 2020, data are available [on this page](https://www.acea.auto/files/ACEA-report-vehicles-in-use-europe-2022.pdf).



## How to cite
Jeannin Noémie, OPENGIS4ET Project WP3


## Authors
Jeannin Noémie <sup>*</sup>

<sup>*</sup> [EPFL, PV-Lab](https://www.epfl.ch/labs/pvlab/), Laboratory of photovoltaics and thin-films electronics, Rue de la Maladière 71b, CH-2002 Neuchâtel 2


## License
Copyright © 2023: Noémie Jeannin
 
Creative Commons Attribution 4.0 International License
This work is licensed under a Creative Commons CC BY 4.0 International License.

SPDX-License-Identifier: CC-BY-4.0

License-Text: https://spdx.org/licenses/CC-BY-4.0.html

## Acknowledgment
We would like to convey our deepest appreciation to the OPENGIS4ET Project, which provided the funding to carry out the present investigation.




